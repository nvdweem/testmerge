#!/usr/bin/env bash

# Set destination branch
DEST_BRANCH=main

# Create new pull request and get its ID
echo "Creating PR: $BITBUCKET_BRANCH -> $DEST_BRANCH"
CREATE_OUTPUT=`curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests \
  --silent \
  --user $BB_USER:$BB_PASSWORD \
  -H 'content-type: application/json' \
  -d '{
    "title": "'$BITBUCKET_BRANCH' -> '$DEST_BRANCH'",
    "description": "automatic PR from pipelines",
    "state": "OPEN",
    "destination": {
      "branch": {
              "name": "'$DEST_BRANCH'"
          }
    },
    "source": {
      "branch": {
              "name": "'$BITBUCKET_BRANCH'"
          }
    }
  }'`

if [[ "$CREATE_OUTPUT" == *"error"* ]]; then
  if [[ "$CREATE_OUTPUT" == *"There are no changes to be pulled"* ]]; then
	echo "Nothing to do, skip"
	exit 0
  fi
  
  ERROR=$(sed -E "s/.*\"message\": \"([^\"]+)\".*/\1/g" <<< $MERGE)
  echo "Send '$ERROR' to slack"
  exit 1
fi
PR_ID=$(sed -E "s/.*\"id\": ([0-9]+).*/\1/g" <<< $CREATE_OUTPUT)

# Merge PR
echo "Merging PR: $PR_ID"
MERGE=`curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests/$PR_ID/merge \
  --show-error --silent \
  --user $BB_USER:$BB_PASSWORD \
  -H 'content-type: application/json' \
  -d '{
    "close_source_branch": false,
    "merge_strategy": "merge_commit"
  }'`

if [[ "$MERGE" == *"error"* ]]; then
  ERROR=$(sed -E "s/.*\"message\": \"([^\"]+)\".*/\1/g" <<< $MERGE)
  echo "Send '$ERROR' to slack"
  exit 2
fi

echo "Merged"